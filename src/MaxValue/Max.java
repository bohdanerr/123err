package MaxValue;

public class Max {
    public static void main(String args[]) {
        int max = Integer.MIN_VALUE;
        int k=0;
        int[] mass = {11, -21, -21, -4, -5, -6, -7, -444, -9};
        for (int i = 0; i < mass.length; i++) {
            if (max < mass[i]) {
                max = mass[i];
                k=i+1;
            }
        }
        System.out.println(max);
        System.out.println(k);
    }
}