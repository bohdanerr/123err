package Calculator;

import java.util.Scanner;

import static java.lang.System.out;

public class Calculator {
    public static void main(String[] args) {
        int f = 0;
        double c1, c2;
        double result;
        String x, y, z;
        final String a = "+";
        final String b = "-";
        final String c = "*";
        final String d = "/";
        String con;
        boolean check = true;

        while (check) {
            out.println("Please enter the first number:");
            while (true) {
                Scanner in = new Scanner(System.in);
                x = in.next();
                if (x.matches("[0-9]*")) {
                    c1 = Integer.parseInt(x);
                    break;
                } else {
                    out.println("You entered not a number, please enter again!");
                }
            }
            //out.println(x);
            out.println("Please enter the second number");
            while (true) {
                Scanner in2 = new Scanner(System.in);
                y = in2.next();
                if (y.matches("[0-9]*")) {
                    c2 = Integer.parseInt(y);
                    break;
                } else {
                    out.println("You entered not a number, please enter again!");
                }
            }
            //out.println(x + y);
            //out.println(c1 + c2);
            out.println("Please select a operation: +, -, *, /");
            Scanner in3 = new Scanner(System.in);
            z = in3.next();
            switch (z) {
                case a:
                    result = c1 + c2;
                    out.println("result=" + result);
                    break;
                case b:
                    result = c1 - c2;
                    out.println("result=" + result);
                    break;
                case c:
                    result = c1 * c2;
                    out.println("result=" + result);
                    break;
                case d:
                    try {
                        result = c1 / c2;
                        out.println("result=" + result);
                    } catch (ArithmeticException e) {
                        out.println("Exception");
                    }
                    break;
                default:
                    out.println("Please enter correct operation");
                    break;
            }
            out.println("Click 'y' to exit?");
            Scanner ch4 = new Scanner(System.in);
            con = ch4.next();
            if (con.equals("y")) {
             break;
            }
        }
    }
}
