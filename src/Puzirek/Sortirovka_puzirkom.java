package Puzirek;

public class Sortirovka_puzirkom {
    public static void main(String args[]) {
        int[] mass = {10, 21, -21, -4, 5, -6, 7, -444, -9};
        int i;
        int v;
        for (int j = 0; j < mass.length - 1; j++) {
            for (i = 0; i < mass.length - 1; i++) {
                if (mass[i] > mass[i + 1]) {
                    v = mass[i];
                    mass[i] = mass[i + 1];
                    mass[i + 1] = v;
                }
            }
        }
        for (i = 0; i < mass.length; i++) {
            System.out.print(mass[i] + " ");
        }
    }
}
